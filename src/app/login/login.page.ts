import { LoadingController, ToastController, Platform } from '@ionic/angular';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('loginForm', null) lForm: NgForm;
  @ViewChild('signupForm', null) sForm: NgForm;
  passwordType: string = 'password';
  passwordShown: boolean = false;
  passwordIcon: string = 'eye-off';
  signIN: boolean = true;
  backButtonSubscription: any;

  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private platform: Platform
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(0, () => {
      navigator['app'].exitApp();
    });
  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  togglePassword() {
    if (this.passwordShown) {
      this.passwordShown = false;
      this.passwordType = 'password';
      this.passwordIcon = 'eye';
    } else {
      this.passwordShown = true;
      this.passwordType = 'string';
      this.passwordIcon = 'eye-off';
    }
  }

  onSignIn() {
    this.signIN = true;
    this.passwordShown = false;
    this.passwordType = 'password';
    this.passwordIcon = 'eye';
  }

  onSignUp() {
    this.signIN = false;
    this.passwordShown = false;
    this.passwordType = 'password';
    this.passwordIcon = 'eye';
  }

  async onSubmit() {
    console.log(this.lForm.value);
    const loading = await this.loadingCtrl.create({
      message: 'Logging in...',
      duration: 5000
    });
    loading.present();

    window.localStorage.setItem('loginCredentials', this.lForm.value);
    this.router.navigateByUrl('/dashboard');
    loading.dismiss();
  }

  async onRegister() {
    if (this.sForm.value.signupPassword === this.sForm.value.confirmPassword && this.sForm.valid) {
      console.log(this.sForm.value);
      this.signIN = true;
      this.passwordShown = false;
      this.passwordType = 'password';
      this.passwordIcon = 'eye';
    } else {
      const toast = await this.toastCtrl.create({
        message: 'Password Mismatch!!',
        duration: 2000
      });
      toast.present();
    }
  }
}
