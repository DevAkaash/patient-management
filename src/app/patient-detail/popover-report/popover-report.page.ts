import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-report',
  templateUrl: './popover-report.page.html',
  styleUrls: ['./popover-report.page.scss'],
})
export class PopoverReportPage implements OnInit {

  imgURL = 'https://upload.wikimedia.org/wikipedia/commons/2/2b/Aire_subdiafragm%C3%A1tico.JPG';

  constructor(
    private popoverCtrl: PopoverController,
    private socialShare: SocialSharing,
  ) { }

  ngOnInit() {
  }

  onPopoverClose() {
    this.popoverCtrl.dismiss();
  }

  onShare() {
    this.socialShare.share().then(() => {
      console.log('Success');
    }).catch(() => {
      console.log('Failed!!!');
    });
  }

  onDelete() {
    //
  }

}
