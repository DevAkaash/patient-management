import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverReportPage } from './popover-report.page';

describe('PopoverReportPage', () => {
  let component: PopoverReportPage;
  let fixture: ComponentFixture<PopoverReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
