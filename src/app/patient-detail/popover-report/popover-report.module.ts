import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverReportPageRoutingModule } from './popover-report-routing.module';

import { PopoverReportPage } from './popover-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverReportPageRoutingModule
  ],
  declarations: [PopoverReportPage]
})
export class PopoverReportPageModule {}
