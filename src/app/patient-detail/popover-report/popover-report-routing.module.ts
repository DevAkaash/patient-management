import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverReportPage } from './popover-report.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverReportPageRoutingModule {}
