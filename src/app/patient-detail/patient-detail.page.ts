import { Component, OnInit } from '@angular/core';
import { PopoverController, LoadingController } from '@ionic/angular';
import { PopoverReportPage } from './popover-report/popover-report.page';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.page.html',
  styleUrls: ['./patient-detail.page.scss'],
})
export class PatientDetailPage implements OnInit {

  base64Image: any;
  flag: any = 1;
  recordDate: any;
  recordType: any;
  type: any[] = ['X-Ray', 'Prescription', 'Blood Test', 'Urine Test', 'Stool Test', 'Other'];
  reportsList: any[] = ['X-Ray', 'Prescription', 'Blood Test', 'Urine Test', 'Stool Test', 'Other'];
  showMenu = false;

  constructor(
    private popoverCtrl: PopoverController,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private socialShare: SocialSharing,
  ) { }

  ngOnInit() {
  }

  onItemClick() {
    if (this.showMenu === false) {
      this.showMenu = true;
    } else {
      this.showMenu = false;
    }
  }

  async onView() {
    const popover = await this.popoverCtrl.create({
      component: PopoverReportPage,
      componentProps: {

      }
    });
    popover.present();
  }

  onShare() {
    this.socialShare.share().then(() => {
      console.log('Success');
    }).catch(() => {
      console.log('Failed!!!');
    });
  }

  onDelete() {
    //
  }

  pickImage(source) {
    const options: CameraOptions = {
      quality: 80,
      sourceType: source,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imgData) => {
        console.log('File Path:', imgData);
        this.base64Image = 'data:image/jpeg;base64,' + imgData;
        this.base64Image = (<any>window).Ionic.WebView.convertFileSrc(imgData);
        console.log('Profile Image: ', this.base64Image);
    }, (err) => {
      console.log(err);
    });
  }

  onCameraSelect() {
    console.log('Camera selected!');
    this.flag = 0;
    this.pickImage(this.camera.PictureSourceType.CAMERA);
  }

  onGallerySelect() {
    console.log('Gallery selected!');
    this.flag = 0;
    this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  async onSave() {
    const loading = await this.loadingCtrl.create({
      message: 'Saving Photo...',
      duration: 2000
    });
    loading.present();
    this.flag = 1;
  }

  onCancel() {
    this.flag = 1;
  }

}
