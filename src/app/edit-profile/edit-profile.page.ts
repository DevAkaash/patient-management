import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  @ViewChild('myaccountForm', null) accountForm: NgForm;
  constructor(
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.accountForm.value);
    this.presentAlert('Information', 'Profile updated successfully.');
  }

  async presentAlert(fetchedHDR: String, fetchedMSG: String) {
    const alert = await this.alertCtrl.create({
      header: `${fetchedHDR}`,
      message: `${fetchedMSG}`,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            return;
          }
        }
      ]
    });
    alert.present();
  }
}
