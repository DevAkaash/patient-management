import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  isLoggedIn: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private menuCtrl: MenuController
  ) {
    this.isLoggedIn = window.localStorage.getItem('loginCredentials');
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.isLoggedIn == null) {
        this.router.navigateByUrl('/login');
      } else {
        this.router.navigateByUrl('/dashboard');
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  onMenuClose() {
    this.menuCtrl.close('m1');
  }

  onMyPatient() {
    this.router.navigateByUrl('/dashboard');
  }

  onMyVideos() {
    this.router.navigateByUrl('/my-video');
  }

  onMyAnalytics() {
    // this.router.navigateByUrl('/')
  }

  onMyAccount() {
    this.router.navigateByUrl('/my-account');
  }
}
