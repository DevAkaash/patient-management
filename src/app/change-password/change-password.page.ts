import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  @ViewChild('changePassword', null) changepassForm: NgForm;
  constructor(
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
  }

  async onSubmit() {
    if (this.changepassForm.value.oldPassword === this.changepassForm.value.newPassword) {
      this.presentToast('Error: New password cannot be same as old password.')
    } else {
      if (this.changepassForm.value.newPassword === this.changepassForm.value.confirmPassword) {
        console.log(this.changepassForm.value);
      } else {
        this.presentToast('Error: Password mismatch!');
      }
    }
  }

  async presentToast(fetchedMSG: String) {
    const toast = await this.toastCtrl.create({
      message: `${fetchedMSG}`,
      duration: 3000
    });
    toast.present();
  }
}
