import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPatientFormPage } from './new-patient-form.page';

const routes: Routes = [
  {
    path: '',
    component: NewPatientFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPatientFormPageRoutingModule {}
