import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewPatientFormPage } from './new-patient-form.page';

describe('NewPatientFormPage', () => {
  let component: NewPatientFormPage;
  let fixture: ComponentFixture<NewPatientFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPatientFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewPatientFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
