import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewPatientFormPageRoutingModule } from './new-patient-form-routing.module';

import { NewPatientFormPage } from './new-patient-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewPatientFormPageRoutingModule
  ],
  declarations: [NewPatientFormPage]
})
export class NewPatientFormPageModule {}
