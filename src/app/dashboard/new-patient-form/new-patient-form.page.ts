import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-new-patient-form',
  templateUrl: './new-patient-form.page.html',
  styleUrls: ['./new-patient-form.page.scss'],
})
export class NewPatientFormPage implements OnInit {

  @ViewChild('patientForm', null) patientForm: NgForm;
  // refDocs: any = ['1', '2', '3', '4', '5'];
  genders: any[] = ['Male', 'Female', 'Other'];
  surgeryStatus: any[] = ['Done', 'Required', 'Not Required'];
  cities: any[] = ['Agra', 'Bhopal', 'Chandigarh', 'Delhi', 'Gantok'];
  problems: any[] = ['TKR', 'THR', 'SCOPY', 'Spine', 'Trauma', 'Others'];

  constructor(
    private router: Router,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.patientForm.value);
    this.presentAlert('Information', 'Patient information added successfully.');
  }

  onCancel() {
    this.patientForm.reset();
    this.router.navigateByUrl('/dashboard');
  }

  async presentAlert(fetchedHDR: String, fetchedMSG: String) {
    const alert = await this.alertCtrl.create({
      header: `${fetchedHDR}`,
      message: `${fetchedMSG}`,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.patientForm.reset();
            return;
          }
        }
      ]
    });
    alert.present();
  }
}
