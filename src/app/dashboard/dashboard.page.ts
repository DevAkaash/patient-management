import { PopoverMapLocationPage } from '../popover-map-location/popover-map-location.page';
import { PopoverController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  dataReturned: any = null;
  backButtonSubscription: any;

  constructor(
    private router: Router,
    private popoverCtrl: PopoverController,
    private loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
  }

  // ngAfterViewInit() {
  //   this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(0, () => {
  //     navigator['app'].exitApp();
  //   });
  // }

  // ngOnDestroy() {
  //   this.backButtonSubscription.unsubscribe();
  // }

  async onPatientCard() {
    const loading = await this.loadingCtrl.create({
      duration: 2000
    });
    loading.present();
    this.router.navigateByUrl('/patient-detail');
  }

  async onMapClick(ev: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverMapLocationPage,
      event: ev
    });

    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        this.dataReturned = dataReturned.data;
      }
    });

    popover.present();
  }

  onSearchChange() {
    //
  }

  onCancelSearchbar() {
    //
  }

  onFabAdd() {
    this.router.navigateByUrl('/dashboard/new-patient-form');
  }

}
