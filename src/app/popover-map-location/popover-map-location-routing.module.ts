import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopoverMapLocationPage } from './popover-map-location.page';

const routes: Routes = [
  {
    path: '',
    component: PopoverMapLocationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopoverMapLocationPageRoutingModule {}
