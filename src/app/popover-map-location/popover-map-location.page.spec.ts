import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverMapLocationPage } from './popover-map-location.page';

describe('PopoverMapLocationPage', () => {
  let component: PopoverMapLocationPage;
  let fixture: ComponentFixture<PopoverMapLocationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverMapLocationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverMapLocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
