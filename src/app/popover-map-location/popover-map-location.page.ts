import { PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popover-map-location',
  templateUrl: './popover-map-location.page.html',
  styleUrls: ['./popover-map-location.page.scss'],
})
export class PopoverMapLocationPage implements OnInit {

  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() {
  }

  async onLocationSelect(flag) {
    if (flag === 1) {
      const data: string = 'Mulund';
      await this.popoverCtrl.dismiss(data);
    } else if (flag === 2) {
      const data: string = 'Sion';
      await this.popoverCtrl.dismiss(data);
    } else if (flag === 3) {
      const data: string = 'Vashi';
      await this.popoverCtrl.dismiss(data);
    } else {
      const data: string = '';
      await this.popoverCtrl.dismiss(data);
    }
  }

}
