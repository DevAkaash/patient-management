import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopoverMapLocationPageRoutingModule } from './popover-map-location-routing.module';

import { PopoverMapLocationPage } from './popover-map-location.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverMapLocationPageRoutingModule
  ],
  declarations: [PopoverMapLocationPage]
})
export class PopoverMapLocationPageModule {}
