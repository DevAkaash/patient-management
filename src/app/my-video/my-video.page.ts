import { PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { PopoverMapLocationPage } from '../popover-map-location/popover-map-location.page';

@Component({
  selector: 'app-my-video',
  templateUrl: './my-video.page.html',
  styleUrls: ['./my-video.page.scss'],
})
export class MyVideoPage implements OnInit {

  dataReturned: any;

  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() {
  }

  async onMapClick(ev: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverMapLocationPage,
      event: ev
    });

    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        this.dataReturned = dataReturned.data;
      }
    });

    popover.present();
  }

  onSearchChange(e) {
    //
  }

  onCancelSearchbar() {
    //
  }

}
