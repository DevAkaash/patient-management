(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["patient-detail-patient-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/patient-detail/patient-detail.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/patient-detail/patient-detail.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\" text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Patient Details</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\" text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Patient Details</ion-title>\n  </ion-toolbar>\n  <ion-card>\n    <ion-item-divider class=\"backgroundTransparent\">Dr. Atul Ranade's Clinic</ion-item-divider>\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-row>\n            <ion-col size=\"3\" class=\"ion-text-center\" style=\"align-self: center;\">\n              <img src=\"/assets/user.png\" />\n            </ion-col>\n            <ion-col size=\"9\">\n              <ion-card-content>\n                <p style=\"font-size: 20px; color: black; font-weight: 700;\">Manish Padhnis</p>\n                <p class=\"textColor\"><ion-text>Age: </ion-text>32</p>\n                <p class=\"textColor\"><ion-text>Sex: </ion-text>Male</p>\n                <p class=\"textColor\"><ion-text>Mobile: </ion-text>9868454210</p>\n                <p class=\"textColor\"><ion-text>D.O.B.: </ion-text>14-10-2000</p>\n              </ion-card-content>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <br>\n  <div *ngIf=\"flag == 1\">\n    <ion-card>\n      <ion-grid style=\"padding: 0\">\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\" style=\"padding: 0\">\n            <ion-item-divider class=\"border1PX\">Reports</ion-item-divider>\n            <ion-list>\n              <!-- <ion-item *ngFor=\"let r of reportsList\"  (click)=\"onItemClick()\" button detail>\n                <ion-label>{{r}}</ion-label>\n                <p>07-09-2019</p>\n              </ion-item> -->\n              <!-- <ion-item-sliding> -->\n                <ion-item (click)=\"onItemClick()\" button detail>\n                  <ion-label>X-Ray Report</ion-label>\n                  <p>07-09-2019</p>\n                </ion-item>\n                <ion-row *ngIf=\"showMenu == true\">\n                  <ion-col>\n                    <ion-button expand=\"block\" color=\"success\" (click)=\"onView()\">\n                      <ion-icon name=\"eye\"></ion-icon>\n                    </ion-button>\n                  </ion-col>\n                  <ion-col>\n                    <ion-button expand=\"block\" color=\"primary\" (click)=\"onShare()\">\n                      <ion-icon name=\"share\"></ion-icon>\n                    </ion-button>\n                  </ion-col>\n                  <ion-col>\n                    <ion-button expand=\"block\" color=\"danger\" (click)=\"onDelete()\">\n                      <ion-icon name=\"trash\"></ion-icon>\n                    </ion-button>\n                  </ion-col>\n                </ion-row>\n                <!-- <ion-item-options side=\"end\">\n                  <ion-item-option color=\"success\" (click)=\"onView()\"><ion-icon name=\"eye\"></ion-icon></ion-item-option>\n                  <ion-item-option color=\"primary\" (click)=\"onShare()\"><ion-icon name=\"share\"></ion-icon></ion-item-option>\n                  <ion-item-option color=\"danger\"(click)=\"onDelete()\"><ion-icon name=\"trash\"></ion-icon></ion-item-option>\n                </ion-item-options> -->\n              <!-- </ion-item-sliding> -->\n              <ion-item-sliding>\n                <ion-item button detail>\n                  <ion-label>Prescription Report</ion-label>\n                  <p>07-09-2019</p>\n                </ion-item>\n                <ion-item-options side=\"end\">\n                  <ion-item-option color=\"success\" (click)=\"onView()\"><ion-icon name=\"eye\"></ion-icon></ion-item-option>\n                  <ion-item-option color=\"primary\" (click)=\"onShare()\"><ion-icon name=\"share\"></ion-icon></ion-item-option>\n                  <ion-item-option color=\"danger\" (click)=\"onDelete()\"><ion-icon name=\"trash\"></ion-icon></ion-item-option>\n                </ion-item-options>\n              </ion-item-sliding>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n  \n  <div *ngIf=\"flag == 0\" class=\"borderRadius8px\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-card class=\"cardstyle\">\n            <ion-row>\n              <ion-col style=\"align-self: center;\">\n                <ion-label>Record Date:</ion-label>\n              </ion-col>\n              <ion-col>\n                <ion-datetime display-format=\"DD-MM-YYYY\" picker-format=\"DD-MM-YYYY\" [(ngModel)]=\"recordDate\" placeholder=\"DD-MM-YYYY\"></ion-datetime>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col style=\"align-self: center;\">\n                <ion-label>Record Type:</ion-label>\n              </ion-col>\n              <ion-col>\n                <ion-select [(ngModel)]=\"recordType\" placeholder=\"Select Record Type\">\n                  <ion-select-option *ngFor=\"let t of type\" [value]=\"t\">{{ t }}</ion-select-option>\n                </ion-select>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <img *ngIf=\"!base64Image\" src=\"https://www.researchgate.net/profile/Mark_Donaldson/publication/299438066/figure/fig1/AS:579836906098688@1515255299891/THE-PERFECT-PRESCRIPTION-FOR-POSTOPERATIVE-DENTAL-PAIN.png\" class=\"centered\" />\n                <img *ngIf=\"base64Image\" [src]=\"base64Image\" class=\"centered\" />\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <ion-button expand=\"block\" fill=\"outline\" color=\"danger\" (click)=\"onCancel()\">Cancel</ion-button>\n              </ion-col>\n              <ion-col>\n                <ion-button expand=\"block\" fill=\"solid\" color=\"secondary\" (click)=\"onSave()\">Save</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" *ngIf=\"flag == 1\">\n    <ion-fab-button>\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"top\">\n      <ion-fab-button>\n        <ion-icon name=\"photos\" color=\"primary\" (click)=\"onGallerySelect()\"></ion-icon>\n      </ion-fab-button>\n      <ion-fab-button>\n        <ion-icon name=\"camera\" color=\"primary\" (click)=\"onCameraSelect()\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab>\n</ion-content>\n\n<!-- <ion-footer *ngIf=\"flag == 1\">\n  <ion-button expand=\"block\" color=\"secondary\" (click)=\"onFabAddReport()\">Add Report</ion-button>\n</ion-footer> -->\n");

/***/ }),

/***/ "./src/app/patient-detail/patient-detail-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/patient-detail/patient-detail-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: PatientDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDetailPageRoutingModule", function() { return PatientDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _patient_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./patient-detail.page */ "./src/app/patient-detail/patient-detail.page.ts");




const routes = [
    {
        path: '',
        component: _patient_detail_page__WEBPACK_IMPORTED_MODULE_3__["PatientDetailPage"]
    },
    {
        path: 'popover-report',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./popover-report/popover-report.module */ "./src/app/patient-detail/popover-report/popover-report.module.ts")).then(m => m.PopoverReportPageModule)
    }
];
let PatientDetailPageRoutingModule = class PatientDetailPageRoutingModule {
};
PatientDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PatientDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/patient-detail/patient-detail.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/patient-detail/patient-detail.module.ts ***!
  \*********************************************************/
/*! exports provided: PatientDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDetailPageModule", function() { return PatientDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _patient_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./patient-detail-routing.module */ "./src/app/patient-detail/patient-detail-routing.module.ts");
/* harmony import */ var _patient_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./patient-detail.page */ "./src/app/patient-detail/patient-detail.page.ts");







let PatientDetailPageModule = class PatientDetailPageModule {
};
PatientDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _patient_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["PatientDetailPageRoutingModule"]
        ],
        declarations: [_patient_detail_page__WEBPACK_IMPORTED_MODULE_6__["PatientDetailPage"]]
    })
], PatientDetailPageModule);



/***/ }),

/***/ "./src/app/patient-detail/patient-detail.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/patient-detail/patient-detail.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('gradientBackdrop.jpg') 0 0/100% 45% no-repeat;\n}\n\nion-toolbar {\n  --background: transparent !important;\n}\n\n.backgroundTransparent {\n  background: transparent;\n  margin-top: 5px;\n  color: white;\n}\n\nion-card {\n  margin: 12px !important;\n}\n\n.cardstyle {\n  background: white;\n}\n\nion-card-content {\n  padding: 5px 5px 5px 5px !important;\n}\n\n.textColor {\n  color: black;\n}\n\n.border1PX {\n  border-bottom: 2px solid #007180;\n}\n\n.centered {\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvSW9uaWNGcmFtZXdvcmsvcGF0aWVudE1hbmFnZW1lbnQvc3JjL2FwcC9wYXRpZW50LWRldGFpbC9wYXRpZW50LWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhdGllbnQtZGV0YWlsL3BhdGllbnQtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdFQUFBO0FDQ0Y7O0FERUE7RUFDRSxvQ0FBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7QUNDRjs7QURFQTtFQUNFLGlCQUFBO0FDQ0Y7O0FERUE7RUFDRSxtQ0FBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtBQ0NGOztBREVBO0VBQ0UsZ0NBQUE7QUNDRjs7QURFQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGF0aWVudC1kZXRhaWwvcGF0aWVudC1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogdXJsKCcuLi8uLi9hc3NldHMvZ3JhZGllbnRCYWNrZHJvcC5qcGcnKSAwIDAvMTAwJSA0NSUgbm8tcmVwZWF0O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYmFja2dyb3VuZFRyYW5zcGFyZW50IHtcclxuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgY29sb3I6IHdoaXRlXHJcbn1cclxuXHJcbmlvbi1jYXJkIHtcclxuICBtYXJnaW46IDEycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNhcmRzdHlsZSB7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbn1cclxuXHJcbmlvbi1jYXJkLWNvbnRlbnQge1xyXG4gIHBhZGRpbmc6IDVweCA1cHggNXB4IDVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dENvbG9yIHtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5ib3JkZXIxUFgge1xyXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCByZ2IoMCwgMTEzLCAxMjgpO1xyXG59XHJcblxyXG4uY2VudGVyZWQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbi8vIGlvbi1mYWIge1xyXG4vLyAgIG1hcmdpbi1sZWZ0OiAzMCU7XHJcbi8vICAgbWFyZ2luLXJpZ2h0OiAzMCU7XHJcbi8vIH1cclxuIiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi4uLy4uL2Fzc2V0cy9ncmFkaWVudEJhY2tkcm9wLmpwZ1wiKSAwIDAvMTAwJSA0NSUgbm8tcmVwZWF0O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLmJhY2tncm91bmRUcmFuc3BhcmVudCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLWNhcmQge1xuICBtYXJnaW46IDEycHggIWltcG9ydGFudDtcbn1cblxuLmNhcmRzdHlsZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogNXB4IDVweCA1cHggNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi50ZXh0Q29sb3Ige1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5ib3JkZXIxUFgge1xuICBib3JkZXItYm90dG9tOiAycHggc29saWQgIzAwNzE4MDtcbn1cblxuLmNlbnRlcmVkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/patient-detail/patient-detail.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/patient-detail/patient-detail.page.ts ***!
  \*******************************************************/
/*! exports provided: PatientDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDetailPage", function() { return PatientDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _popover_report_popover_report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./popover-report/popover-report.page */ "./src/app/patient-detail/popover-report/popover-report.page.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");






let PatientDetailPage = class PatientDetailPage {
    constructor(popoverCtrl, camera, loadingCtrl, socialShare) {
        this.popoverCtrl = popoverCtrl;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.socialShare = socialShare;
        this.flag = 1;
        this.type = ['X-Ray', 'Prescription', 'Blood Test', 'Urine Test', 'Stool Test', 'Other'];
        this.reportsList = ['X-Ray', 'Prescription', 'Blood Test', 'Urine Test', 'Stool Test', 'Other'];
        this.showMenu = false;
    }
    ngOnInit() {
    }
    onItemClick() {
        if (this.showMenu === false) {
            this.showMenu = true;
        }
        else {
            this.showMenu = false;
        }
    }
    onView() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _popover_report_popover_report_page__WEBPACK_IMPORTED_MODULE_3__["PopoverReportPage"],
                componentProps: {}
            });
            popover.present();
        });
    }
    onShare() {
        this.socialShare.share().then(() => {
            console.log('Success');
        }).catch(() => {
            console.log('Failed!!!');
        });
    }
    onDelete() {
        //
    }
    pickImage(source) {
        const options = {
            quality: 80,
            sourceType: source,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imgData) => {
            console.log('File Path:', imgData);
            this.base64Image = 'data:image/jpeg;base64,' + imgData;
            this.base64Image = window.Ionic.WebView.convertFileSrc(imgData);
            console.log('Profile Image: ', this.base64Image);
        }, (err) => {
            console.log(err);
        });
    }
    onCameraSelect() {
        console.log('Camera selected!');
        this.flag = 0;
        this.pickImage(this.camera.PictureSourceType.CAMERA);
    }
    onGallerySelect() {
        console.log('Gallery selected!');
        this.flag = 0;
        this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    }
    onSave() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Saving Photo...',
                duration: 2000
            });
            loading.present();
            this.flag = 1;
        });
    }
    onCancel() {
        this.flag = 1;
    }
};
PatientDetailPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__["SocialSharing"] }
];
PatientDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-patient-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./patient-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/patient-detail/patient-detail.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./patient-detail.page.scss */ "./src/app/patient-detail/patient-detail.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__["SocialSharing"]])
], PatientDetailPage);



/***/ })

}]);