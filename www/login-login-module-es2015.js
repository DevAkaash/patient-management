(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>login</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"setpadding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <img\n          src=\"/assets/DoctorLogo.jpg\"\n          height=\"200px\"\n          width=\"auto\"\n          class=\"center\"\n        />\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-card class=\"cardSwitcher\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"4\" offset-sm=\"4\">\n          <ion-row>\n            <ion-col style=\"text-align: right; border-right: 1px solid\" (click)=\"onSignIn()\" class=\"ion-activatable\">\n              <ion-label style=\"padding-right: 5px;\">Sign-In</ion-label>\n              <ion-ripple-effect></ion-ripple-effect>\n            </ion-col>\n            <ion-col style=\"border-left: 1px solid\" (click)=\"onSignUp()\" class=\"ion-activatable\">\n              <ion-label style=\"padding-left: 5px;\">Sign-Up</ion-label>\n              <ion-ripple-effect></ion-ripple-effect>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card *ngIf=\"signIN == true\">\n    <ion-item-divider color=\"secondary\">Login</ion-item-divider>\n    <form #loginForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-list>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Mobile No.</ion-label>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-input\n                      placeholder=\"Enter Phone Number\"\n                      type=\"tel\"\n                      ngModel\n                      name=\"loginPhone\"\n                      required\n                      minlength=\"10\"\n                      maxlength=\"10\"\n                      #phoneCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <p *ngIf=\"!phoneCtrl.valid && phoneCtrl.touched\" class=\"ion-text-right\">\n                Invalid Phone Number!\n              </p>\n              <div class=\"setLine\"></div>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Password</ion-label>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <ion-input\n                      placeholder=\"Enter Password\"\n                      [type]=\"passwordType\"\n                      ngModel\n                      name=\"loginPassword\"\n                      required\n                      minlength=\"6\"\n                      maxlength=\"16\"\n                      clearOnEdit=\"false\"\n                      #passwordCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <div class=\"lefticon\" (click)=\"togglePassword()\">\n                      <ion-icon\n                        [name]=\"passwordIcon\"\n                        color=\"dark\"\n                        slot=\"icon-only\"\n                      ></ion-icon>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <p *ngIf=\"!passwordCtrl.valid && passwordCtrl.touched\" class=\"ion-text-right\">\n                Invalid Password!\n              </p>\n              <div class=\"setLine\"></div>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n        <!-- <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-row>\n              <ion-col size=\"1\">\n                <ion-checkbox color=\"tertiary\" class=\"setterms\"></ion-checkbox>\n              </ion-col>\n              <ion-col no-padding>\n                <p style=\"color: white\" class=\"settext\" (click)=\"ontandc()\">\n                  <u>Terms &amp; Condition</u>\n                </p>\n              </ion-col>\n              <ion-col no-padding>\n                <p style=\"color: white\" class=\"ion-text-right\">\n                  <u>Privacy Policy</u>\n                </p>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row> -->\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-button\n              class=\"loginBtn\"\n              type=\"submit\"\n              expand=\"block\"\n              [disabled]=\"!loginForm.valid\">\n              Login\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </ion-card>\n  <!-- Card -->\n  <ion-card *ngIf=\"signIN == false\">\n    <ion-item-divider color=\"secondary\">Sign Up</ion-item-divider>\n    <form #signupForm=\"ngForm\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-list>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Name</ion-label>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-input\n                      placeholder=\"Enter Name\"\n                      type=\"text\"\n                      ngModel\n                      name=\"signupName\"\n                      required\n                      #nameCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <p *ngIf=\"!nameCtrl.valid && nameCtrl.touched\" class=\"ion-text-right\">\n                Name field cannot be empty.\n              </p>\n              <div class=\"setLine\"></div>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Mobile No.</ion-label>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-input\n                      placeholder=\"Enter Phone Number\"\n                      type=\"tel\"\n                      ngModel\n                      name=\"signupPhone\"\n                      required\n                      minlength=\"10\"\n                      maxlength=\"10\"\n                      #phoneCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <p *ngIf=\"!phoneCtrl.valid && phoneCtrl.touched\" class=\"ion-text-right\">\n                Invalid Phone Number!\n              </p>\n              <div class=\"setLine\"></div>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Password</ion-label>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <ion-input\n                      placeholder=\"Enter Password\"\n                      [type]=\"passwordType\"\n                      ngModel\n                      name=\"signupPassword\"\n                      required\n                      minlength=\"6\"\n                      maxlength=\"16\"\n                      clearOnEdit=\"false\"\n                      #passwordCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <div class=\"lefticon\" (click)=\"togglePassword()\">\n                      <ion-icon\n                        [name]=\"passwordIcon\"\n                        color=\"dark\"\n                        slot=\"icon-only\"\n                      ></ion-icon>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <p *ngIf=\"!passwordCtrl.valid && passwordCtrl.touched\" class=\"ion-text-right\">\n                Invalid Password!\n              </p>\n              <div class=\"setLine\"></div>\n              <div>\n                <ion-row>\n                  <ion-col>\n                    <ion-label>Confirm Password</ion-label>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                    <ion-input\n                      placeholder=\"Confirm Password\"\n                      [type]=\"passwordType\"\n                      ngModel\n                      name=\"confirmPassword\"\n                      required\n                      minlength=\"6\"\n                      maxlength=\"16\"\n                      clearOnEdit=\"false\"\n                      #passwordCtrl=\"ngModel\"\n                    ></ion-input>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <div class=\"lefticon\" (click)=\"togglePassword()\">\n                      <ion-icon\n                        [name]=\"passwordIcon\"\n                        color=\"dark\"\n                        slot=\"icon-only\"\n                      ></ion-icon>\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </div>\n              <div class=\"setLine\"></div>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-button\n              class=\"loginBtn\"\n              expand=\"block\"\n              (click)=\"onRegister()\">\n              Sign-Up\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".center {\n  display: block;\n  margin-top: 30px;\n  margin-left: auto;\n  margin-right: auto;\n  width: 55%;\n  height: auto;\n}\n\nion-icon {\n  height: 28px;\n  width: 28px;\n}\n\n.lefticon {\n  margin-top: 5px;\n  margin-left: 8px;\n}\n\np.ion-text-right {\n  color: red;\n  margin-bottom: 0px;\n}\n\nion-card {\n  background: aliceblue;\n}\n\nion-list {\n  background: transparent;\n}\n\n.setpadding {\n  --padding-start: 0px !important;\n  --padding-end: 0px !important;\n  --padding-top: 0px !important;\n  --background: url('abstractBackdrop.jpg') 0 0/100% 100%\n    no-repeat;\n}\n\n.setLine {\n  border: 0.5px solid #c5e3fd;\n  margin: 1px 0px 3px 0px;\n}\n\n.cardSwitcher {\n  margin: 0px 100px 5px 100px;\n  border-radius: 30px;\n}\n\n.ion-activatable {\n  position: relative;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvSW9uaWNGcmFtZXdvcmsvcGF0aWVudE1hbmFnZW1lbnQvc3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxxQkFBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7QUNDRjs7QURFQTtFQUNFLCtCQUFBO0VBQ0EsNkJBQUE7RUFDQSw2QkFBQTtFQUVBO2FBQUE7QUNDRjs7QURHQTtFQUNFLDJCQUFBO0VBQ0EsdUJBQUE7QUNBRjs7QURHQTtFQUNFLDJCQUFBO0VBQ0EsbUJBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIHdpZHRoOiA1NSU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgaGVpZ2h0OiAyOHB4O1xyXG4gIHdpZHRoOiAyOHB4O1xyXG59XHJcblxyXG4ubGVmdGljb24ge1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxuICBtYXJnaW4tbGVmdDogOHB4O1xyXG59XHJcblxyXG5wLmlvbi10ZXh0LXJpZ2h0IHtcclxuICBjb2xvcjogcmVkO1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuaW9uLWNhcmQge1xyXG4gIGJhY2tncm91bmQ6IGFsaWNlYmx1ZTtcclxufVxyXG5cclxuaW9uLWxpc3Qge1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4uc2V0cGFkZGluZyB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHggIWltcG9ydGFudDtcclxuICAtLXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDtcclxuICAtLXBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcclxuICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9sb2dpbl9iYWNrLnBuZycpIG5vLXJlcGVhdCAxMDAlIDEwMCU7XHJcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi9hc3NldHMvYWJzdHJhY3RCYWNrZHJvcC5qcGdcIikgMCAwLzEwMCUgMTAwJVxyXG4gICAgbm8tcmVwZWF0O1xyXG59XHJcblxyXG4uc2V0TGluZSB7XHJcbiAgYm9yZGVyOiAwLjVweCBzb2xpZCByZ2IoMTk3LCAyMjcsIDI1Myk7XHJcbiAgbWFyZ2luOiAxcHggMHB4IDNweCAwcHg7XHJcbn1cclxuXHJcbi5jYXJkU3dpdGNoZXIge1xyXG4gIG1hcmdpbjogMHB4IDEwMHB4IDVweCAxMDBweDtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG59XHJcblxyXG4uaW9uLWFjdGl2YXRhYmxlIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4iLCIuY2VudGVyIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIHdpZHRoOiA1NSU7XG4gIGhlaWdodDogYXV0bztcbn1cblxuaW9uLWljb24ge1xuICBoZWlnaHQ6IDI4cHg7XG4gIHdpZHRoOiAyOHB4O1xufVxuXG4ubGVmdGljb24ge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA4cHg7XG59XG5cbnAuaW9uLXRleHQtcmlnaHQge1xuICBjb2xvcjogcmVkO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbmlvbi1jYXJkIHtcbiAgYmFja2dyb3VuZDogYWxpY2VibHVlO1xufVxuXG5pb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uc2V0cGFkZGluZyB7XG4gIC0tcGFkZGluZy1zdGFydDogMHB4ICFpbXBvcnRhbnQ7XG4gIC0tcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi9hc3NldHMvYWJzdHJhY3RCYWNrZHJvcC5qcGdcIikgMCAwLzEwMCUgMTAwJVxuICAgIG5vLXJlcGVhdDtcbn1cblxuLnNldExpbmUge1xuICBib3JkZXI6IDAuNXB4IHNvbGlkICNjNWUzZmQ7XG4gIG1hcmdpbjogMXB4IDBweCAzcHggMHB4O1xufVxuXG4uY2FyZFN3aXRjaGVyIHtcbiAgbWFyZ2luOiAwcHggMTAwcHggNXB4IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4uaW9uLWFjdGl2YXRhYmxlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let LoginPage = class LoginPage {
    constructor(router, loadingCtrl, toastCtrl, platform) {
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.passwordType = 'password';
        this.passwordShown = false;
        this.passwordIcon = 'eye-off';
        this.signIN = true;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(0, () => {
            navigator['app'].exitApp();
        });
    }
    ngOnDestroy() {
        this.backButtonSubscription.unsubscribe();
    }
    togglePassword() {
        if (this.passwordShown) {
            this.passwordShown = false;
            this.passwordType = 'password';
            this.passwordIcon = 'eye';
        }
        else {
            this.passwordShown = true;
            this.passwordType = 'string';
            this.passwordIcon = 'eye-off';
        }
    }
    onSignIn() {
        this.signIN = true;
        this.passwordShown = false;
        this.passwordType = 'password';
        this.passwordIcon = 'eye';
    }
    onSignUp() {
        this.signIN = false;
        this.passwordShown = false;
        this.passwordType = 'password';
        this.passwordIcon = 'eye';
    }
    onSubmit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(this.lForm.value);
            const loading = yield this.loadingCtrl.create({
                message: 'Logging in...',
                duration: 5000
            });
            loading.present();
            window.localStorage.setItem('loginCredentials', this.lForm.value);
            this.router.navigateByUrl('/dashboard');
            loading.dismiss();
        });
    }
    onRegister() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.sForm.value.signupPassword === this.sForm.value.confirmPassword && this.sForm.valid) {
                console.log(this.sForm.value);
                this.signIN = true;
                this.passwordShown = false;
                this.passwordType = 'password';
                this.passwordIcon = 'eye';
            }
            else {
                const toast = yield this.toastCtrl.create({
                    message: 'Password Mismatch!!',
                    duration: 2000
                });
                toast.present();
            }
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('loginForm', null),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
], LoginPage.prototype, "lForm", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('signupForm', null),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
], LoginPage.prototype, "sForm", void 0);
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"]])
], LoginPage);



/***/ })

}]);