function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDashboardDashboardPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"m1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Dr. Name</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onMapClick($event)\">\n        <ion-label *ngIf=\"dataReturned != null\">{{dataReturned}}</ion-label>\n        <ion-icon name=\"pin\" slot=\"start\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"searchToolbar\">\n    <ion-searchbar animated=\"true\" placeholder=\"Search Customer Mobile No.\" inputmode=\"tel\" debounce=\"1000\" ngModel name=\"searchInput\" (ionChange)=\"onSearchChange($event)\" (ionCancel)=\"onCancelSearchbar()\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"setpadding\">\n  <ion-card (click)=\"onPatientCard()\">\n    <ion-card-content>\n      <ion-grid class=\"ion-no-padding\">\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-row>\n              <ion-col size=\"3\" class=\"ion-text-center\" style=\"align-self: center;\">\n                <img src=\"/assets/user.png\" />\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-label>\n                  <ion-row>\n                    <ion-col size=\"7\">\n                      <h3 style=\"color: black; font-weight: 700;\">Manish Padhnis</h3>\n                    </ion-col>\n                    <ion-col>\n                      <h3 class=\"ion-text-end\" style=\"color: darkgray; font-weight: 700;\">Male, 32</h3>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Ref. Doc:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dr. Kalpana Shah</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Surgery Status:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Done, THR</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Location:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dombivali</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Last Visited:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>07-09-2019</h3></ion-col>\n                  </ion-row>\n                </ion-label>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <ion-card (click)=\"onPatientCard()\">\n    <ion-card-content>\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-row>\n              <ion-col size=\"3\" class=\"ion-text-center\" style=\"align-self: center;\">\n                <img src=\"/assets/user.png\" />\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-label>\n                  <ion-row>\n                    <ion-col size=\"7\">\n                      <h3 style=\"color: black; font-weight: 700;\">Manish Padhnis</h3>\n                    </ion-col>\n                    <ion-col>\n                      <h3 class=\"ion-text-end\" style=\"color: darkgray; font-weight: 700;\">Male, 32</h3>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Ref. Doc:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dr. Kalpana Shah</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Surgery Status:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Done, THR</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Location:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dombivali</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Last Visited:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>07-09-2019</h3></ion-col>\n                  </ion-row>\n                </ion-label>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <ion-card (click)=\"onPatientCard()\">\n    <ion-card-content>\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-row>\n              <ion-col size=\"3\" class=\"ion-text-center\" style=\"align-self: center;\">\n                <img src=\"/assets/user.png\" />\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-label>\n                  <ion-row>\n                    <ion-col size=\"7\">\n                      <h3 style=\"color: black; font-weight: 700;\">Manish Padhnis</h3>\n                    </ion-col>\n                    <ion-col>\n                      <h3 class=\"ion-text-end\" style=\"color: darkgray; font-weight: 700;\">Male, 32</h3>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Ref. Doc:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dr. Kalpana Shah</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Surgery Status:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Done, THR</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Location:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dombivali</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Last Visited:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>07-09-2019</h3></ion-col>\n                  </ion-row>\n                </ion-label>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <ion-card>\n    <ion-card-content>\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col size-sm=\"6\" offset-sm=\"3\">\n            <ion-row>\n              <ion-col size=\"3\" class=\"ion-text-center\" style=\"align-self: center;\">\n                <img src=\"/assets/user.png\" />\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-label>\n                  <ion-row>\n                    <ion-col size=\"7\">\n                      <h3 style=\"color: black; font-weight: 700;\">Manish Padhnis</h3>\n                    </ion-col>\n                    <ion-col>\n                      <h3 class=\"ion-text-end\" style=\"color: darkgray; font-weight: 700;\">Male, 32</h3>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Ref. Doc:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dr. Kalpana Shah</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Surgery Status:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Done, THR</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Location:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>Dombivali</h3></ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col class=\"paddingZero\"><h3 style=\"color: black\">Last Visited:</h3></ion-col>\n                    <ion-col class=\"paddingZero\"><h3>07-09-2019</h3></ion-col>\n                  </ion-row>\n                </ion-label>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  \n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"onFabAdd()\">\n      <ion-icon name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: DashboardPageRoutingModule */

  /***/
  function srcAppDashboardDashboardRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function () {
      return DashboardPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./dashboard.page */
    "./src/app/dashboard/dashboard.page.ts");

    var routes = [{
      path: '',
      component: _dashboard_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPage"]
    }, {
      path: 'new-patient-form',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | new-patient-form-new-patient-form-module */
        "new-patient-form-new-patient-form-module").then(__webpack_require__.bind(null,
        /*! ./new-patient-form/new-patient-form.module */
        "./src/app/dashboard/new-patient-form/new-patient-form.module.ts")).then(function (m) {
          return m.NewPatientFormPageModule;
        });
      }
    }];

    var DashboardPageRoutingModule = function DashboardPageRoutingModule() {
      _classCallCheck(this, DashboardPageRoutingModule);
    };

    DashboardPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], DashboardPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/dashboard/dashboard.module.ts ***!
    \***********************************************/

  /*! exports provided: DashboardPageModule */

  /***/
  function srcAppDashboardDashboardModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function () {
      return DashboardPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./dashboard-routing.module */
    "./src/app/dashboard/dashboard-routing.module.ts");
    /* harmony import */


    var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./dashboard.page */
    "./src/app/dashboard/dashboard.page.ts");

    var DashboardPageModule = function DashboardPageModule() {
      _classCallCheck(this, DashboardPageModule);
    };

    DashboardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"]],
      declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })], DashboardPageModule);
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/dashboard/dashboard.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppDashboardDashboardPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header-ios ion-toolbar:last-child {\n  padding-top: 14px;\n  --border-width: 0 !important;\n}\n\n.searchToolbar {\n  --background: lightblue !important;\n}\n\n.setpadding {\n  --background: url('backgroundMedical.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-card {\n  margin: 12px !important;\n  border-left: 3px solid #007180;\n}\n\nion-card-content {\n  padding: 5px 5px 5px 5px !important;\n}\n\nimg {\n  width: 100%;\n  height: auto;\n}\n\n.paddingZero {\n  padding: 0px 0px 0px 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvSW9uaWNGcmFtZXdvcmsvcGF0aWVudE1hbmFnZW1lbnQvc3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBQTtFQUNBLDRCQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQ0FBQTtBQ0NGOztBREVBO0VBSUUsa0VBQUE7QUNGRjs7QURLQTtFQUNFLHVCQUFBO0VBQ0EsOEJBQUE7QUNGRjs7QURLQTtFQUNFLG1DQUFBO0FDRkY7O0FEU0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQ05GOztBRFNBO0VBQ0Usd0JBQUE7QUNORiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlci1pb3MgaW9uLXRvb2xiYXI6bGFzdC1jaGlsZCB7XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgLS1ib3JkZXItd2lkdGg6IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnNlYXJjaFRvb2xiYXIge1xyXG4gIC0tYmFja2dyb3VuZDogbGlnaHRibHVlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zZXRwYWRkaW5nIHtcclxuICAvLyAtLXBhZGRpbmctc3RhcnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gIC8vIC0tcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xyXG4gIC8vIC0tcGFkZGluZy10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gIC0tYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vYXNzZXRzL2JhY2tncm91bmRNZWRpY2FsLmpwZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxufVxyXG5cclxuaW9uLWNhcmQge1xyXG4gIG1hcmdpbjogMTJweCAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1sZWZ0OiAzcHggc29saWQgcmdiKDAsIDExMywgMTI4KTtcclxufVxyXG5cclxuaW9uLWNhcmQtY29udGVudCB7XHJcbiAgcGFkZGluZzogNXB4IDVweCA1cHggNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIGgye1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyB9XHJcblxyXG5pbWcge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogYXV0bztcclxufVxyXG5cclxuLnBhZGRpbmdaZXJvIHtcclxuICBwYWRkaW5nOiAwcHggMHB4IDBweCA1cHg7XHJcbn1cclxuIiwiLmhlYWRlci1pb3MgaW9uLXRvb2xiYXI6bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICAtLWJvcmRlci13aWR0aDogMCAhaW1wb3J0YW50O1xufVxuXG4uc2VhcmNoVG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogbGlnaHRibHVlICFpbXBvcnRhbnQ7XG59XG5cbi5zZXRwYWRkaW5nIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi9hc3NldHMvYmFja2dyb3VuZE1lZGljYWwuanBnXCIpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24tY2FyZCB7XG4gIG1hcmdpbjogMTJweCAhaW1wb3J0YW50O1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICMwMDcxODA7XG59XG5cbmlvbi1jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiA1cHggNXB4IDVweCA1cHggIWltcG9ydGFudDtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cblxuLnBhZGRpbmdaZXJvIHtcbiAgcGFkZGluZzogMHB4IDBweCAwcHggNXB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/dashboard/dashboard.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/dashboard/dashboard.page.ts ***!
    \*********************************************/

  /*! exports provided: DashboardPage */

  /***/
  function srcAppDashboardDashboardPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPage", function () {
      return DashboardPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _popover_map_location_popover_map_location_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../popover-map-location/popover-map-location.page */
    "./src/app/popover-map-location/popover-map-location.page.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var DashboardPage =
    /*#__PURE__*/
    function () {
      function DashboardPage(router, popoverCtrl, loadingCtrl) {
        _classCallCheck(this, DashboardPage);

        this.router = router;
        this.popoverCtrl = popoverCtrl;
        this.loadingCtrl = loadingCtrl;
        this.dataReturned = null;
      }

      _createClass(DashboardPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // ngAfterViewInit() {
        //   this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(0, () => {
        //     navigator['app'].exitApp();
        //   });
        // }
        // ngOnDestroy() {
        //   this.backButtonSubscription.unsubscribe();
        // }

      }, {
        key: "onPatientCard",
        value: function onPatientCard() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var loading;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingCtrl.create({
                      duration: 2000
                    });

                  case 2:
                    loading = _context.sent;
                    loading.present();
                    this.router.navigateByUrl('/patient-detail');

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "onMapClick",
        value: function onMapClick(ev) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var popover;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.popoverCtrl.create({
                      component: _popover_map_location_popover_map_location_page__WEBPACK_IMPORTED_MODULE_1__["PopoverMapLocationPage"],
                      event: ev
                    });

                  case 2:
                    popover = _context2.sent;
                    popover.onDidDismiss().then(function (dataReturned) {
                      if (dataReturned != null) {
                        _this.dataReturned = dataReturned.data;
                      }
                    });
                    popover.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "onSearchChange",
        value: function onSearchChange() {//
        }
      }, {
        key: "onCancelSearchbar",
        value: function onCancelSearchbar() {//
        }
      }, {
        key: "onFabAdd",
        value: function onFabAdd() {
          this.router.navigateByUrl('/dashboard/new-patient-form');
        }
      }]);

      return DashboardPage;
    }();

    DashboardPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }];
    };

    DashboardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
      selector: 'app-dashboard',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dashboard.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dashboard.page.scss */
      "./src/app/dashboard/dashboard.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])], DashboardPage);
    /***/
  }
}]);
//# sourceMappingURL=dashboard-dashboard-module-es5.js.map