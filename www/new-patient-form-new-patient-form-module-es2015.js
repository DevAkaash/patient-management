(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-patient-form-new-patient-form-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/new-patient-form/new-patient-form.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/new-patient-form/new-patient-form.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\" text=\"\" color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Please Enter Patient Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <form (ngSubmit)=\"onSubmit()\" #patientForm=\"ngForm\">\n          <!-- <ion-card> -->\n            <ion-list>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\" class=\"labelHeader\">First Name*</ion-label>\n                <ion-input type=\"text\" ngModel name=\"firstname\" required></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Last Name*</ion-label>\n                <ion-input type=\"text\" ngModel name=\"lastname\" required></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Mobile No.*</ion-label>\n                <ion-input type=\"tel\" ngModel name=\"mobileno\" required></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Date of Birth*</ion-label>\n                <ion-datetime display-format=\"DD-MM-YYYY\" picker-format=\"DD-MM-YYYY\" ngModel name=\"dob\" required></ion-datetime>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">E-mail ID</ion-label>\n                <ion-input \n                  type=\"email\" pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" \n                  ngModel name=\"email\">\n                </ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Ref. Doctor*</ion-label>\n                <ion-input type=\"text\" ngModel name=\"refDoctor\" required></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Location*</ion-label>\n                <ion-input type=\"text\" ngModel name=\"location\" required></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">City*</ion-label>\n                <ion-select ngModel name=\"city\" required>\n                  <ion-select-option *ngFor=\"let city of cities\" [value]=\"city\">{{ city }}</ion-select-option>\n                </ion-select>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Sex*</ion-label>\n                <ion-select ngModel name=\"sex\" required>\n                  <ion-select-option *ngFor=\"let g of genders\" [value]=\"g\">{{ g }}</ion-select-option>\n                </ion-select>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Surgery</ion-label>\n                <ion-select ngModel name=\"surgery\">\n                  <ion-select-option *ngFor=\"let status of surgeryStatus\" [value]=\"status\">{{ status }}</ion-select-option>\n                </ion-select>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\" class=\"labelHeader\">Problem Area</ion-label>\n                <ion-select ngModel name=\"problems\" multiple=\"true\">\n                  <ion-select-option *ngFor=\"let p of problems\" [value]=\"p\">{{ p }}</ion-select-option>\n                </ion-select>\n              </ion-item>\n            </ion-list>\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"6\">\n                  <ion-button expand=\"block\" fill=\"outline\" color=\"danger\" (click)=\"onCancel()\">Cancel</ion-button>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-button expand=\"block\" color=\"secondary\" type=\"submit\" [disabled]=\"!patientForm.valid\">Save</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          <!-- </ion-card> -->\n        </form>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/dashboard/new-patient-form/new-patient-form-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/dashboard/new-patient-form/new-patient-form-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: NewPatientFormPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewPatientFormPageRoutingModule", function() { return NewPatientFormPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _new_patient_form_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-patient-form.page */ "./src/app/dashboard/new-patient-form/new-patient-form.page.ts");




const routes = [
    {
        path: '',
        component: _new_patient_form_page__WEBPACK_IMPORTED_MODULE_3__["NewPatientFormPage"]
    }
];
let NewPatientFormPageRoutingModule = class NewPatientFormPageRoutingModule {
};
NewPatientFormPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NewPatientFormPageRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/new-patient-form/new-patient-form.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/dashboard/new-patient-form/new-patient-form.module.ts ***!
  \***********************************************************************/
/*! exports provided: NewPatientFormPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewPatientFormPageModule", function() { return NewPatientFormPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _new_patient_form_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-patient-form-routing.module */ "./src/app/dashboard/new-patient-form/new-patient-form-routing.module.ts");
/* harmony import */ var _new_patient_form_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./new-patient-form.page */ "./src/app/dashboard/new-patient-form/new-patient-form.page.ts");







let NewPatientFormPageModule = class NewPatientFormPageModule {
};
NewPatientFormPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _new_patient_form_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewPatientFormPageRoutingModule"]
        ],
        declarations: [_new_patient_form_page__WEBPACK_IMPORTED_MODULE_6__["NewPatientFormPage"]]
    })
], NewPatientFormPageModule);



/***/ }),

/***/ "./src/app/dashboard/new-patient-form/new-patient-form.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/dashboard/new-patient-form/new-patient-form.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".labelHeader {\n  font-weight: bold;\n  color: #243150;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvSW9uaWNGcmFtZXdvcmsvcGF0aWVudE1hbmFnZW1lbnQvc3JjL2FwcC9kYXNoYm9hcmQvbmV3LXBhdGllbnQtZm9ybS9uZXctcGF0aWVudC1mb3JtLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL25ldy1wYXRpZW50LWZvcm0vbmV3LXBhdGllbnQtZm9ybS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUJBO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0FDeEJGIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL25ldy1wYXRpZW50LWZvcm0vbmV3LXBhdGllbnQtZm9ybS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpb24tY29udGVudCB7XHJcbi8vICAgLS1iYWNrZ3JvdW5kOiByZ2IoMzYsIDQ5LCA4MCk7XHJcbi8vIH1cclxuXHJcbi8vIGlvbi1pbnB1dCB7XHJcbi8vICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDM2LCA0OSwgODApO1xyXG4vLyAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuLy8gICBwYWRkaW5nLWxlZnQ6IDE4cHggIWltcG9ydGFudDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLWRhdGV0aW1lIHtcclxuLy8gICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMzYsIDQ5LCA4MCk7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tc2VsZWN0IHtcclxuLy8gICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMzYsIDQ5LCA4MCk7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tY2FyZCB7XHJcbi8vICAgbWFyZ2luOiAxMnB4ICFpbXBvcnRhbnQ7XHJcbi8vICAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCByZ2IoMCwgMTEzLCAxMjgpO1xyXG4vLyB9XHJcblxyXG4ubGFiZWxIZWFkZXIge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGNvbG9yOiByZ2IoMzYsIDQ5LCA4MClcclxufVxyXG5cclxuIiwiLmxhYmVsSGVhZGVyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMjQzMTUwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/dashboard/new-patient-form/new-patient-form.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/dashboard/new-patient-form/new-patient-form.page.ts ***!
  \*********************************************************************/
/*! exports provided: NewPatientFormPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewPatientFormPage", function() { return NewPatientFormPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let NewPatientFormPage = class NewPatientFormPage {
    constructor(router) {
        this.router = router;
        // refDocs: any = ['1', '2', '3', '4', '5'];
        this.genders = ['Male', 'Female', 'Other'];
        this.surgeryStatus = ['Done', 'Required', 'Not Required'];
        this.cities = ['1', '2', '3', '4', '5'];
        this.problems = ['TKR', 'THR', 'SCOPY', 'Spine', 'Trauma', 'Others'];
    }
    ngOnInit() {
    }
    onSubmit() {
        console.log(this.patientForm.value);
    }
    onCancel() {
        this.patientForm.reset();
        this.router.navigateByUrl('/dashboard');
    }
};
NewPatientFormPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('patientForm', null),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"])
], NewPatientFormPage.prototype, "patientForm", void 0);
NewPatientFormPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-new-patient-form',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-patient-form.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/new-patient-form/new-patient-form.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-patient-form.page.scss */ "./src/app/dashboard/new-patient-form/new-patient-form.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], NewPatientFormPage);



/***/ })

}]);