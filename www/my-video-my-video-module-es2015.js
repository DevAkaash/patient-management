(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-video-my-video-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/my-video/my-video.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-video/my-video.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"m1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Dr. Name</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onMapClick($event)\">\n        <ion-label *ngIf=\"dataReturned != null\">{{dataReturned}}</ion-label>\n        <ion-icon name=\"pin\" slot=\"start\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"searchToolbar\">\n    <ion-searchbar animated=\"true\" placeholder=\"Search Customer Mobile No.\" inputmode=\"tel\" debounce=\"1000\" ngModel name=\"searchInput\" (ionChange)=\"onSearchChange($event)\" (ionCancel)=\"onCancelSearchbar()\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content> </ion-content>\n");

/***/ }),

/***/ "./src/app/my-video/my-video-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/my-video/my-video-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: MyVideoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideoPageRoutingModule", function() { return MyVideoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _my_video_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-video.page */ "./src/app/my-video/my-video.page.ts");




const routes = [
    {
        path: '',
        component: _my_video_page__WEBPACK_IMPORTED_MODULE_3__["MyVideoPage"]
    }
];
let MyVideoPageRoutingModule = class MyVideoPageRoutingModule {
};
MyVideoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyVideoPageRoutingModule);



/***/ }),

/***/ "./src/app/my-video/my-video.module.ts":
/*!*********************************************!*\
  !*** ./src/app/my-video/my-video.module.ts ***!
  \*********************************************/
/*! exports provided: MyVideoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideoPageModule", function() { return MyVideoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _my_video_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-video-routing.module */ "./src/app/my-video/my-video-routing.module.ts");
/* harmony import */ var _my_video_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-video.page */ "./src/app/my-video/my-video.page.ts");







let MyVideoPageModule = class MyVideoPageModule {
};
MyVideoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_video_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyVideoPageRoutingModule"]
        ],
        declarations: [_my_video_page__WEBPACK_IMPORTED_MODULE_6__["MyVideoPage"]]
    })
], MyVideoPageModule);



/***/ }),

/***/ "./src/app/my-video/my-video.page.scss":
/*!*********************************************!*\
  !*** ./src/app/my-video/my-video.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header-ios ion-toolbar:last-child {\n  padding-top: 14px;\n  --border-width: 0 !important;\n}\n\n.searchToolbar {\n  --background: lightblue !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ha2Fhc2hkZXYvUHJvZ3JhbW1pbmcvSW9uaWNGcmFtZXdvcmsvcGF0aWVudE1hbmFnZW1lbnQvc3JjL2FwcC9teS12aWRlby9teS12aWRlby5wYWdlLnNjc3MiLCJzcmMvYXBwL215LXZpZGVvL215LXZpZGVvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0VBQ0EsNEJBQUE7QUNDRjs7QURFQTtFQUNFLGtDQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9teS12aWRlby9teS12aWRlby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWlvcyBpb24tdG9vbGJhcjpsYXN0LWNoaWxkIHtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICAtLWJvcmRlci13aWR0aDogMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc2VhcmNoVG9vbGJhciB7XHJcbiAgLS1iYWNrZ3JvdW5kOiBsaWdodGJsdWUgIWltcG9ydGFudDtcclxufVxyXG4iLCIuaGVhZGVyLWlvcyBpb24tdG9vbGJhcjpsYXN0LWNoaWxkIHtcbiAgcGFkZGluZy10b3A6IDE0cHg7XG4gIC0tYm9yZGVyLXdpZHRoOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5zZWFyY2hUb29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaWdodGJsdWUgIWltcG9ydGFudDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/my-video/my-video.page.ts":
/*!*******************************************!*\
  !*** ./src/app/my-video/my-video.page.ts ***!
  \*******************************************/
/*! exports provided: MyVideoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideoPage", function() { return MyVideoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _popover_map_location_popover_map_location_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../popover-map-location/popover-map-location.page */ "./src/app/popover-map-location/popover-map-location.page.ts");




let MyVideoPage = class MyVideoPage {
    constructor(popoverCtrl) {
        this.popoverCtrl = popoverCtrl;
    }
    ngOnInit() {
    }
    onMapClick(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _popover_map_location_popover_map_location_page__WEBPACK_IMPORTED_MODULE_3__["PopoverMapLocationPage"],
                event: ev
            });
            popover.onDidDismiss().then((dataReturned) => {
                if (dataReturned != null) {
                    this.dataReturned = dataReturned.data;
                }
            });
            popover.present();
        });
    }
    onSearchChange(e) {
        //
    }
    onCancelSearchbar() {
        //
    }
};
MyVideoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"] }
];
MyVideoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-my-video',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./my-video.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/my-video/my-video.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./my-video.page.scss */ "./src/app/my-video/my-video.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"]])
], MyVideoPage);



/***/ })

}]);